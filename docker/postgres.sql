create user personservice with encrypted password 'personservice';
create database personservice;
grant all privileges on database personservice to personservice;
create database personservicetest;
grant all privileges on database personservicetest to personservice;