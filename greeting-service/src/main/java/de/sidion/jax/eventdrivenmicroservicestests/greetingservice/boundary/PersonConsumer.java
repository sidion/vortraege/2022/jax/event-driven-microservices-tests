package de.sidion.jax.eventdrivenmicroservicestests.greetingservice.boundary;

import de.sidion.jax.eventdrivenmicroservicestests.greetingservice.control.PersonRepository;
import de.sidion.jax.eventdrivenmicroservicestests.greetingservice.entity.Person;
import org.eclipse.microprofile.reactive.messaging.Incoming;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class PersonConsumer {

    @Inject PersonRepository personRepository;

    @Incoming("person")
    void consumePersonEvent(Person person) {
        if (personRepository.get(person.getId()).isPresent()) {
            personRepository.remove(person.getId());
        }
        personRepository.add(person);
    }
}
