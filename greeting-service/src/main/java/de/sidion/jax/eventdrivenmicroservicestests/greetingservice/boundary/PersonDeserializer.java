package de.sidion.jax.eventdrivenmicroservicestests.greetingservice.boundary;

import de.sidion.jax.eventdrivenmicroservicestests.greetingservice.entity.Person;
import io.quarkus.kafka.client.serialization.ObjectMapperDeserializer;

public class PersonDeserializer extends ObjectMapperDeserializer<Person> {
    public PersonDeserializer() {
        super(Person.class);
    }
}
