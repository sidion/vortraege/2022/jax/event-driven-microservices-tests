package de.sidion.jax.eventdrivenmicroservicestests.greetingservice.control;

import de.sidion.jax.eventdrivenmicroservicestests.greetingservice.entity.Person;
import io.quarkus.logging.Log;

import javax.enterprise.context.ApplicationScoped;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@ApplicationScoped
public class PersonRepository {

    private final Set<Person> personSet = new HashSet<>();

    public void add(Person person) {
        personSet.add(person);
        Log.info("add person: " + person.toString());
    }

    public void remove(Long id) {
        Log.infov("remove person [{0}]", id);
        personSet.removeIf(p -> id.equals(p.getId()));
    }

    public Optional<Person> get(Long id) {
        return personSet.stream().filter(e -> id.equals(e.getId())).findFirst();
    }

    public Set<Person> get(String email) {
        return personSet.stream()
                .filter(e -> email.equals(e.getEmail()))
                .collect(Collectors.toUnmodifiableSet());
    }

    public Set<Person> get() {
        return personSet;
    }
}
