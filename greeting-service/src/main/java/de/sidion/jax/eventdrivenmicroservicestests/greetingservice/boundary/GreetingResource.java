package de.sidion.jax.eventdrivenmicroservicestests.greetingservice.boundary;

import de.sidion.jax.eventdrivenmicroservicestests.greetingservice.control.PersonRepository;
import de.sidion.jax.eventdrivenmicroservicestests.greetingservice.entity.Person;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Path("/greeting")
public class GreetingResource {
    @Inject PersonRepository personRepository;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello(@QueryParam("id") Long id, @QueryParam("email") String email) {
        if (id != null) {
            var person = personRepository.get(id);
            if (person.isPresent()) {
                return createMessage(person.get());
            }
        }
        if (email != null) {
            var personSet = personRepository.get(email);
            if (!personSet.isEmpty()) {
                return createMessage(personSet.stream().findFirst().get());
            }
        }
        return "Hello JAX";
    }

    private String createMessage(Person person) {
        return String.format("Hello %s %s!", person.getForename(), person.getSurname());
    }
}
