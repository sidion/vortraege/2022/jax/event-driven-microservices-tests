Feature: Greeting

  Background:
    * def KafkaTestHelper = Java.type('de.sidion.jax.eventdrivenmicroservicestests.greetingservice.boundary.KafkaTestHelper')
    * def kafka = new KafkaTestHelper()
    * def topic = "person"
    * configure retry = { count: 10, interval: 2000 }


  Scenario: consume person changed event and greet them

    Given json personWithBirthdayEvent = read('events/PersonChanged/PersonWithBirthday.json')
    And print '### personWithBirthdayEvent:', personWithBirthdayEvent
    * string payload = personWithBirthdayEvent.payload
    And kafka.publish(topic, payload, personWithBirthdayEvent.header)
    And url applicationBaseUrl
    And path 'greeting'
    And param email = 'helena.adam@sidion.de'
    And retry until response != 'Hello JAX'
    When method get
    Then match response == 'Hello Helena Adam!'
    And print '### greeting:', response
    And kafka.close()

