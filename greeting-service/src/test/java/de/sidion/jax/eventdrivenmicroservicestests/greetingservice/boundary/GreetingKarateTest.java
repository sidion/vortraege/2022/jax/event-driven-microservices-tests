package de.sidion.jax.eventdrivenmicroservicestests.greetingservice.boundary;

import com.intuit.karate.junit5.Karate;
import io.quarkus.test.junit.QuarkusTest;

@QuarkusTest
public class GreetingKarateTest {

    @Karate.Test
    Karate consumeEvent() {
        return Karate.run("classpath:karate/Greeting.feature");
    }
}
