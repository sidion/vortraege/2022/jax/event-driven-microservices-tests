package de.sidion.jax.eventdrivenmicroservicestests.greetingservice.boundary;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.common.serialization.Serdes;

import java.util.Properties;

public class KafkaFactory {

    public static KafkaProducer<String, String> createProducer() {
        return new KafkaProducer<>(
                loadProperties(), Serdes.String().serializer(), Serdes.String().serializer());
    }

    private static Properties loadProperties() {
        var props = new Properties();
        props.put("bootstrap.servers", "localhost:9093");
        return props;
    }
}
