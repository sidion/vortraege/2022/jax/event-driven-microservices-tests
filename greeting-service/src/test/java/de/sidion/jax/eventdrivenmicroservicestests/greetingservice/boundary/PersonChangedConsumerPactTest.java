package de.sidion.jax.eventdrivenmicroservicestests.greetingservice.boundary;

import au.com.dius.pact.consumer.MessagePactBuilder;
import au.com.dius.pact.consumer.dsl.PactDslJsonBody;
import au.com.dius.pact.consumer.junit5.PactConsumerTestExt;
import au.com.dius.pact.consumer.junit5.PactTestFor;
import au.com.dius.pact.consumer.junit5.ProviderType;
import au.com.dius.pact.core.model.PactSpecVersion;
import au.com.dius.pact.core.model.annotations.Pact;
import au.com.dius.pact.core.model.messaging.MessagePact;
import de.sidion.jax.eventdrivenmicroservicestests.ConsumerVerifier;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.util.HashMap;
import java.util.Map;

@ExtendWith(PactConsumerTestExt.class)
@PactTestFor(providerName = "person-service-person-producer", providerType = ProviderType.ASYNCH)
class PersonChangedConsumerPactTest {


    @Pact(consumer = "greeting-service-person-consumer")
    MessagePact createPact(MessagePactBuilder builder) {
        PactDslJsonBody body = new PactDslJsonBody();
        body.integerType("id", 1L);
        body.stringType("surname", "Adam");
        body.stringType("forename", "Helena");
        body.stringType("email", "helena.adam@sidion.de");
        body.stringType("birthday", "1984-08-06");

        Map<String, Object> metadata = new HashMap<>();
        metadata.put("Content-Type", "application/json");
        metadata.put("kafka_topic", "person");
        metadata.put("action", "post");
        metadata.put("event", "PersonChanged");
        metadata.put("providerState", "PersonChanged");

        return builder
                .given("PersonWithBirthday")
                .expectsToReceive("PersonChanged")
                .withMetadata(metadata)
                .withContent(body)
                .toPact();
    }

    @Test
    @PactTestFor(
            pactMethod = "createPact",
            providerType = ProviderType.ASYNCH,
            pactVersion = PactSpecVersion.V3)
    void verifyPersonWithBirthday(MessagePact messagePact) {
        new ConsumerVerifier("PersonChanged", "PersonWithBirthday").verify(messagePact);
    }
}
