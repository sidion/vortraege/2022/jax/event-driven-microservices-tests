package de.sidion.jax.eventdrivenmicroservicestests;

import au.com.dius.pact.core.model.messaging.MessagePact;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.fail;
import static org.assertj.core.api.Assertions.assertThat;

public class ConsumerVerifier {

    private static final Path EVENTS_PATH =
            Paths.get("src", "test", "resources", "events");

    private final String eventName;
    private final String providerState;
    private final Map<String, Object> header;
    private final JsonNode payload;
    private final ObjectMapper mapper;

    public ConsumerVerifier(final String eventName, final String providerState) {

        this.eventName = eventName;
        this.providerState = providerState;
        mapper = new ObjectMapper();
        final Path eventPath = EVENTS_PATH.resolve(eventName).resolve(providerState + ".json");

        final JsonNode event;
        try {
            event = mapper.readTree(eventPath.toFile());
        } catch (final IOException e) {
            header = null;
            payload = null;
            fail("Event " + eventName + " could not be read for provider State: " + providerState);
            return;
        }
        header = new HashMap<>();

        final JsonNode eventHeader = event.get("header");
        eventHeader
                .fieldNames()
                .forEachRemaining(name -> header.put(name, eventHeader.get(name).textValue()));
        payload = event.get("payload");
    }

    public void verify(final MessagePact messagePact) {

        final byte[] pactMessage = messagePact.getMessages().get(0).contentsAsBytes();
        final Map<String, Object> pactMetadata = messagePact.getMessages().get(0).getMetadata();
        try{
            // verify message content with provider state event payload
            assertThat(mapper.readTree(pactMessage)).isEqualTo(payload);
        } catch (IOException e) {
            fail("Not parsable json event " + eventName + " for provider State: " + providerState);
        }
        assertThat(pactMetadata).isEqualTo(header);

    }

}
