package de.sidion.jax.eventdrivenmicroservicestests.boundary;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.sidion.jax.eventdrivenmicroservicestests.entity.Person;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

@QuarkusTest
public class PersonResourceTest {

  @Test
  public void testPersonEndpoint() throws URISyntaxException, IOException {

    // given
    var objectMapper = new ObjectMapper();
    var personTypeReference = new TypeReference<List<Person>>() {};

    var addPersonFile =
        new File(getClass().getClassLoader().getResource("data/CreatePersonWithBirthday.json").getFile());
    var addPersonJson = Files.readString(addPersonFile.toPath());

    // when
    var body =
        given().when().contentType(ContentType.JSON).body(addPersonJson).post("/person").getBody();

    // then
    var expectedPersonFile =
        new File(getClass().getClassLoader().getResource("data/CreatePersonWithBirthdayExpected.json").getFile());
    var expectedPerson = objectMapper.readValue(expectedPersonFile, Person.class);
    var personsResponse = objectMapper.readValue(body.print(), personTypeReference);
    assertThat(personsResponse).isNotEmpty();
    var personResponse = personsResponse.get(0);
    expectedPerson.setId(personResponse.getId());
    assertThat(personResponse).isEqualTo(expectedPerson);
  }
}
