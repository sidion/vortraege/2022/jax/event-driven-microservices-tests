package de.sidion.jax.eventdrivenmicroservicestests.boundary;

import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.Serdes;

import java.util.Properties;

public class KafkaFactory {

    public static KafkaConsumer<String, String> createConsumer() {
        return new KafkaConsumer<>(
                loadProperties(), Serdes.String().deserializer(), Serdes.String().deserializer());
    }

    private static Properties loadProperties() {
        var props = new Properties();
        props.put("bootstrap.servers", "localhost:9093");
        props.put("group.id", "kafka-test");
        return props;
    }
}
