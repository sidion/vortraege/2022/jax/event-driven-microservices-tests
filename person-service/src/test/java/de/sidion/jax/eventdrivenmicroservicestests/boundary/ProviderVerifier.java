package de.sidion.jax.eventdrivenmicroservicestests.boundary;

import au.com.dius.pact.core.model.messaging.MessagePact;
import au.com.dius.pact.provider.MessageAndMetadata;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class ProviderVerifier {

    private static final Path EVENTS_PATH =
            Paths.get("src", "test", "resources", "events");

    private final String eventName;
    private final String providerState;
    private final Map<String, Object> header;
    private final JsonNode payload;
    private final ObjectMapper mapper;

    public ProviderVerifier(final String eventName, final String providerState) {

        this.eventName = eventName;
        this.providerState = providerState;
        mapper = getStrictedObjectMapper();
        final Path eventPath = EVENTS_PATH.resolve(eventName).resolve(providerState + ".json");

        final JsonNode event;
        try {
            event = mapper.readTree(eventPath.toFile());
        } catch (final IOException e) {
            header = null;
            payload = null;
            fail("Event " + eventName + " could not be read for provider State: " + providerState);
            return;
        }
        header = new HashMap<>();

        final JsonNode eventHeader = event.get("header");
        eventHeader
                .fieldNames()
                .forEachRemaining(name -> header.put(name, eventHeader.get(name).textValue()));
        payload = event.get("payload");
    }

    public MessageAndMetadata toMessageAndMetadata(){
        try{
            return  new MessageAndMetadata(mapper.writeValueAsBytes(payload), header);
        } catch (JsonProcessingException e) {
            fail("MessageAndMetadata could not be created for Event " + eventName + " and provider State: " + providerState);
            return null;
        }
    }

    public void verify(final Class<?> type) {

        final Object object;

        try{
            object = mapper.readValue(payload.toString(), type);
        } catch (final Exception e) {
            fail("Not parsable json event " + eventName + " for provider State: " + providerState+ ", Exception:" + e.getMessage());
        }

    }

    private ObjectMapper getStrictedObjectMapper() {

        final ObjectMapper strictedMapper = new ObjectMapper();
        strictedMapper.configure(DeserializationFeature.FAIL_ON_IGNORED_PROPERTIES, true);
        strictedMapper.configure(DeserializationFeature.FAIL_ON_INVALID_SUBTYPE, true);
        strictedMapper.configure(DeserializationFeature.FAIL_ON_READING_DUP_TREE_KEY, true);
        strictedMapper.configure(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES, true);
        strictedMapper.configure(DeserializationFeature.FAIL_ON_NULL_CREATOR_PROPERTIES, true);
        strictedMapper.configure(DeserializationFeature.FAIL_ON_TRAILING_TOKENS, true);
        return strictedMapper;
    }

}
