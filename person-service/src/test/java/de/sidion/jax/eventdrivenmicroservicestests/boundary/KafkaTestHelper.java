package de.sidion.jax.eventdrivenmicroservicestests.boundary;

import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.time.Duration;
import java.util.Collections;

public class KafkaTestHelper {
    private static final Duration TIMEOUT = Duration.ofSeconds(40);
    private final KafkaConsumer<String, String> kafkaConsumer;

    public KafkaTestHelper() {
        kafkaConsumer = KafkaFactory.createConsumer();
    }

    public static <T> T getLastElement(final Iterable<T> elements) {
        T lastElement = null;

        for (T element : elements) {
            lastElement = element;
        }

        return lastElement;
    }

    public void close() {
        kafkaConsumer.close();
    }

    public String consume(String topic) {
        kafkaConsumer.subscribe(Collections.singletonList(topic));
        try {
            var records = kafkaConsumer.poll(TIMEOUT);
            var recordsIterable = records.records(topic);
            var lastRecord = getLastElement(recordsIterable);
            return lastRecord != null ? lastRecord.value() : null;
        } finally {
            kafkaConsumer.commitSync();
            kafkaConsumer.unsubscribe();
        }
    }
}
