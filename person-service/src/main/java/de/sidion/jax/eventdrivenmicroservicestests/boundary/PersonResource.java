package de.sidion.jax.eventdrivenmicroservicestests.boundary;

import de.sidion.jax.eventdrivenmicroservicestests.entity.Person;

import javax.inject.Inject;
import javax.ws.rs.*;
import java.util.List;

@Path("/person")
public class PersonResource {

    @Inject PersonBean personBean;

    @GET
    public List<Person> list() {
        return Person.findAll().list();
    }

    @GET
    @Path("{id}")
    public Person getById(@PathParam("id") Long id) {
        return Person.findById(id);
    }

    @POST
    public List<Person> post(List<Person> persons) {
        persons.forEach(
                person -> {
                    personBean.add(person);
                    personBean.sendEvent(person, "created");
                });
        return persons;
    }

    @PATCH
    public Person patch(Person person) {
        var entity = personBean.patch(person);
        personBean.sendEvent(entity, "updated");
        return entity;
    }
}
