package de.sidion.jax.eventdrivenmicroservicestests.boundary;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.sidion.jax.eventdrivenmicroservicestests.entity.Person;
import io.smallrye.reactive.messaging.kafka.api.OutgoingKafkaRecordMetadata;
import org.apache.kafka.common.header.internals.RecordHeaders;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.eclipse.microprofile.reactive.messaging.Message;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;

@ApplicationScoped
public class PersonBean {

    @Channel("person")
    Emitter<Person> personEmitter;

    @Transactional
    public void add(Person person) {
        person.persistAndFlush();
    }

    @Transactional
    public Person patch(Person person) {
        Person entity = Person.findById(person.getId());
        if (entity != null) {
            entity.setForename(person.getForename());
            entity.setSurname(person.getSurname());
            entity.setEmail(person.getEmail());
            entity.setPhone(person.getPhone());
            entity.persistAndFlush();
        }
        return entity;
    }

    public void sendEvent(Person person, String action) {

        ObjectMapper mapper = new ObjectMapper();

        var message =
                Message.of(person)
                        .addMetadata(
                                OutgoingKafkaRecordMetadata.<String>builder()
                                        .withKey(person.getEmail())
                                        .withHeaders(
                                                new RecordHeaders()
                                                        .add(
                                                                "action",
                                                                action.getBytes(
                                                                        StandardCharsets.UTF_8)))
                                        .build());
        personEmitter.send(message);
        message.ack().toCompletableFuture().orTimeout(10, TimeUnit.SECONDS).join();
    }
}
