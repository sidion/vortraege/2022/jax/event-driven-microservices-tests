# Event-driven Microservices-Tests – wie die CD Pipeline mit CDC und BDD die E2E-Tests ablöst

Demo Projekt für den Vortrag 'Event-driven Microservices-Tests' auf der JAX 2022

Link zur
Session: https://jax.de/devops-continuous-delivery/event-driven-microservices-tests-wie-die-cd-pipeline-mit-cdc-und-bdd-die-e2e-tests-abloest/

### Start

```shell
mvn clean verify
```

--> error: no pacts found

publish pacts in greeting service

## Greeting Service

### Publish PACTs

```shell
mvn clean verify pact:publish
```

## Person Service

### Verify PACTs

```shell
mvn verify -Dpact.provider.version=1.0-SNAPSHOT -Dpact.verifier.publishResults=true
```